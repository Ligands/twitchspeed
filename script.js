// use javascript to control the player's playback rate directly
// eg. document.getElementsByTagName("video")[0].playbackRate = 1.0
//
// Comma keycode = 188, period = 190, ; = 59, equals = 61, f = 70, / = 191 

var speed = 1.0;
var ff = false;

function decreasePlaybackRate(){
	speed -= 0.25;
	// just in case there's floating-point rounding issues
	if(speed < 1) speed = 1;
	setSpeed(speed);
}
function increasePlaybackRate(){
	speed += 0.25;
	// just in case there's floating-point rounding issues
	if(speed > 2) speed = 2;
	setSpeed(speed);
}
function setSpeed(amt){
	document.getElementsByTagName("video")[0].playbackRate = amt;
}

document.addEventListener('keydown', function(event){
	//console.log(event.keyCode);});
	if(event.keyCode == 188){
		decreasePlaybackRate();
	} else if(event.keyCode == 190){
		increasePlaybackRate();
	} else if(event.keyCode == 61){
		speed = 1;
		setSpeed(speed);
	} else if(event.keyCode == 59 && !ff){
		setSpeed(3);
		ff = true;
	}
});

document.addEventListener('keyup', function(event){
	if(event.keyCode == 59){
		setSpeed(speed);
		ff = false;
	}
});