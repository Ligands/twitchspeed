Browser plugin (firefox/chrome) to add youtube's keyboard shortcuts for changing playback speed of (non-live) Twitch videos.

Use the angle brackets (Shift + Comma & Shift + Period) to adjust plyaback speed in increments of 0.25x.
Use the equals button to reset the playback rate to normal.
Hold down the semicolon key for a 'fast-forward', setting playback to 3x temporarily while button is held